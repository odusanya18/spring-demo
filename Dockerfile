FROM openjdk:12-jdk-alpine
VOLUME /tmp
COPY /out/artifacts/demo_jar/demo.jar app.jar
COPY /greeting.yaml greeting.yaml
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]