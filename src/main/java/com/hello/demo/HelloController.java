package com.hello.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(@RequestParam(value="name", defaultValue="World") String name) {
        return String.format("Hello %s!", name);
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public String health() {
        return "Ok";
    }
}
